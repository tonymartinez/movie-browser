//
//  OMBDConfig.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation

struct OMBDConfig: WebServiceConfiguration {
    var baseURL: URL {
        return URL(string: "http://www.omdbapi.com")!
    }
    
    var apiKey: String {
        return "bd9d5d71"
    }
    
    var sessionConfiguration: URLSessionConfiguration {
        let session = URLSessionConfiguration.default
        session.timeoutIntervalForRequest = 60
        
        return session
    }
}

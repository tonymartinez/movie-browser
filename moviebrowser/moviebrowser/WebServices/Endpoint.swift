//
//  Endpoint.swift
//  moviebrowser
//
//  Created by Tony Martínez on 31/10/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Alamofire

protocol Endpoint {
    var method: HTTPMethod { get }
    var params: [String: Any]? { get }
}

//
//  NetworkClient.swift
//  moviebrowser
//
//  Created by Tony Martínez on 31/10/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import RxSwift

class WebService: SessionManager {
    private let baseURL: URL
    private let apiKey: String
    private let responseQueue = DispatchQueue(label: "response-queue",
                                              qos: .utility,
                                              attributes: [.concurrent])
    
    init(configuration: WebServiceConfiguration) {
        self.baseURL = configuration.baseURL
        self.apiKey = configuration.apiKey
        super.init(configuration: configuration.sessionConfiguration)
    }
    
    func rx_requestObject<T: BaseMappable>(_ endpoint: Endpoint) -> Maybe<T> {
        let req = request(for: endpoint)
        return Maybe<T>.create { [unowned self] observer in

            req.validate().responseObject(queue: self.responseQueue, completionHandler: { (response: DataResponse<T>) in
                if response.result.isSuccess, let value = response.result.value {
                    observer(.success(value))
                } else if let error = response.error {
                    observer(.error(error))
                } else {
                    observer(.completed)
                }
            })
                
            return Disposables.create {
                req.cancel()
            }
        }
    }
    
    func rx_requestArray<T: BaseMappable>(_ endpoint: Endpoint) -> Maybe<[T]> {
        let req = request(for: endpoint)
        return Maybe<[T]>.create { [unowned self] observer in
            
            req.validate().responseArray(queue: self.responseQueue, completionHandler: { (response: DataResponse<[T]>) in
                if response.result.isSuccess, let value = response.result.value {
                    observer(.success(value))
                } else if let error = response.error {
                    observer(.error(error))
                } else {
                    observer(.completed)
                }
            })
                
            return Disposables.create {
                req.cancel()
            }
        }
    }
}

extension WebService {
    
    private func request(for endpoint: Endpoint) -> DataRequest {
        var parameters = endpoint.params ?? [:]
        parameters["apikey"] = apiKey
        
        return request(baseURL,
                       method: endpoint.method,
                       parameters: parameters)
    }
}

//
//  WebServiceConfiguration.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation

protocol WebServiceConfiguration {
    var baseURL: URL { get }
    var apiKey: String { get }
    var sessionConfiguration: URLSessionConfiguration { get }
}

//
//  SearchResponse.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import ObjectMapper

class SearchResponse: Mappable {
    private(set) var movies: [Movie] = []
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        movies <- map["Search"]
    }
}

//
//  SearchEndpoint.swift
//  moviebrowser
//
//  Created by Tony Martínez on 31/10/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import Alamofire

struct SearchEndpoint: Endpoint {
    private let query: String
    
    init(query: String) {
        self.query = query
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var params: [String : Any]? {
        return [
            "s": query
        ]
    }
}

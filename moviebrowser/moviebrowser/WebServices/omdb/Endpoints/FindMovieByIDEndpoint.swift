//
//  FindMovieByIDEndpoint.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import Alamofire

struct FindMovieByIDEndpoint: Endpoint {
    private let id: String
    
    init(id: String) {
        self.id = id
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var params: [String : Any]? {
        return [
            "i": id,
            "plot": "full"
        ]
    }
}

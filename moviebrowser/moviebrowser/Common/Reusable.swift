//
//  Reusable.swift
//  moviebrowser
//
//  Created by Tony Martínez on 31/10/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation

protocol Reusable {
    static var reuseID: String {get}
}

extension Reusable {
    static var reuseID: String {
        return String(describing: self)
    }
}

//
//  MovieDetailWireframeType.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import UIKit

protocol MovieDetailWireframeType {
    func createModule(with movie: Movie) -> UIViewController
    func goToWebsite(_ movie: Movie)
    func share(_ items: [Any])
    func showPoster(_ movie: Movie)
}

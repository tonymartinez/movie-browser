//
//  MovieDetailInteractorType.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import RxSwift

protocol MovieDetailInteractorType {
    var movie: Movie { get }
    
    func movieDetail() -> Observable<Movie>
    func share() -> Observable<[Any]>
}

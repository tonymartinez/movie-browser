//
//  MovieDetailPresenterType.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct MovieDetailPresenterInput {
    let triggerDetail: Driver<Void>
    let triggerWebsite: Driver<Void>
    let triggerShare: Driver<Void>
    let posterPressed: Driver<Void>
}

struct MovieDetailPresenterOutput {
    let loading: Driver<Bool>
    let title: Driver<String>
    let image: Driver<URL?>
    let released: Driver<String>
    let duration: Driver<String>
    let genre: Driver<String>
    let plot: Driver<String>
    let showError: Driver<Error>
    let availableWebsite: Driver<Bool>
}

protocol MovieDetailPresenterType {
    func transform(input: MovieDetailPresenterInput) -> MovieDetailPresenterOutput
}

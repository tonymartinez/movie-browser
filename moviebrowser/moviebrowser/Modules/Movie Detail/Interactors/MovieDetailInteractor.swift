//
//  MovieDetailInteractor.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import RxSwift

class MovieDetailInteractor: MovieDetailInteractorType {
    private(set) var movie: Movie
    private let service: WebService
    
    init(movie: Movie, service: WebService) {
        self.movie = movie
        self.service = service
    }
    
    func movieDetail() -> Observable<Movie> {
        let endpoint = FindMovieByIDEndpoint(id: movie.imdbID)
        return service.rx_requestObject(endpoint)
            .asObservable()
            .do(onNext: { [weak self] in
                self?.movie = $0
            })
            .startWith(movie)
    }
    
    func share() -> Observable<[Any]> {
        guard let web = movie.details?.web else { return Observable.just([]) }
        return Observable.just([movie.title, web])
    }
}

//
//  MovieDetailWireframe.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import UIKit

class MovieDetailWireframe: MovieDetailWireframeType {
    private let navigator: UINavigationController
    private let service: WebService
    
    init(navigator: UINavigationController, service: WebService) {
        self.navigator = navigator
        self.service = service
    }
    
    func createModule(with movie: Movie) -> UIViewController {
        let interactor = MovieDetailInteractor(movie: movie, service: service)
        let presenter = MovieDetailPresenter(interactor: interactor, wireframe: self)
        let view = MovieDetailViewController(presenter: presenter)
        
        return view
    }
    
    func goToWebsite(_ movie: Movie) {
        guard let website = movie.details?.web else { return }
        UIApplication.shared.open(website)
    }
    
    func share(_ items: [Any]) {
        let view = UIActivityViewController(activityItems: items, applicationActivities: nil)
        navigator.present(view, animated: true, completion: nil)
    }
    
    func showPoster(_ movie: Movie) {
        let nav = UINavigationController()
        let wireframe = PosterPreviewWireframe(navigator: nav)
        let view = wireframe.createModule(movie: movie)
        
        nav.viewControllers = [view]
        nav.modalTransitionStyle = .crossDissolve
        navigator.present(nav, animated: true, completion: nil)
    }
}

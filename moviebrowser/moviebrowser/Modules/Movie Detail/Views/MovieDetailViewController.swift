//
//  MovieDetailViewController.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxGesture
import Kingfisher

class MovieDetailViewController: UIViewController {
    private let presenter: MovieDetailPresenterType
    private let bag = DisposeBag()
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releasedLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var plotLabel: UILabel!
    
    @IBOutlet weak var websiteButton: UIButton!
    init(presenter: MovieDetailPresenterType) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        configureView()
        bindPresenter()
    }
    
    deinit {
        imageView.kf.cancelDownloadTask()
    }
    
    private func configureNavigationBar() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Share",
                                                            style: .plain,
                                                            target: nil,
                                                            action: nil)
    }
    
    private func configureView() {
        scrollView.refreshControl = UIRefreshControl()
        websiteButton.setTitle("Go to website", for: .normal)
        //imageView.isUserInteractionEnabled = true
    }
    
    private func bindPresenter() {
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        
        let pull = scrollView.refreshControl!.rx
            .controlEvent(.valueChanged)
            .asDriver()
        
        let gotoWebsite = websiteButton.rx.tap.asDriver().mapToVoid()
        let share = navigationItem.rightBarButtonItem!.rx.tap.asDriver().mapToVoid()
        let posterPressed = imageView.rx.tapGesture().when(.recognized)
            .asDriverOnErrorJustComplete()
            .mapToVoid()
        
        let input = MovieDetailPresenterInput(triggerDetail: Driver.merge(viewWillAppear, pull),
                                              triggerWebsite: gotoWebsite,
                                              triggerShare: share,
                                              posterPressed: posterPressed)
        let output = presenter.transform(input: input)
        
        output.loading.drive(scrollView.refreshControl!.rx.isRefreshing).disposed(by: bag)
        output.title.drive(titleLabel.rx.text).disposed(by: bag)
        output.released.drive(releasedLabel.rx.text).disposed(by: bag)
        output.duration.drive(durationLabel.rx.text).disposed(by: bag)
        output.genre.drive(genreLabel.rx.text).disposed(by: bag)
        output.plot.drive(plotLabel.rx.text).disposed(by: bag)
        output.image.asObservable().subscribe(onNext: showImage).disposed(by: bag)
        output.availableWebsite.drive(navigationItem.rightBarButtonItem!.rx.isEnabled).disposed(by: bag)
        output.availableWebsite.map { !$0 }.drive(websiteButton.rx.isHidden).disposed(by: bag)
        
        output.showError
            .drive(onNext: showError)
            .disposed(by: bag)
    }
    
    private func showImage(_ url: URL?) {
        let processor = DownsamplingImageProcessor(size: imageView.bounds.size)
        imageView.kf.setImage(
            with: url,
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(0.5))
            ])
    }
}

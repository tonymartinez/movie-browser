//
//  MovieDetailPresenter.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class MovieDetailPresenter: MovieDetailPresenterType {
    private let interactor: MovieDetailInteractorType
    private let wireframe: MovieDetailWireframeType
    private let bag = DisposeBag()
    
    init(interactor: MovieDetailInteractorType,
         wireframe: MovieDetailWireframeType) {
        self.interactor = interactor
        self.wireframe = wireframe
    }
    
    func transform(input: MovieDetailPresenterInput) -> MovieDetailPresenterOutput {
        let activityTracker = ActivityTracker()
        let errorTracker = ErrorTracker()
        
        let details = input.triggerDetail.flatMapLatest { [unowned self] in
            return self.interactor.movieDetail()
                .trackActivity(activityTracker)
                .trackError(errorTracker)
                .asDriver(onErrorJustReturn: self.interactor.movie)
        }
        
        input.triggerWebsite
            .asObservable()
            .withLatestFrom(Observable.just(interactor.movie))
            .subscribe(onNext: wireframe.goToWebsite)
            .disposed(by: bag)
        
        input.triggerShare
            .asObservable()
            .flatMapLatest { [unowned self] in
                return self.interactor.share()
        }.subscribe(onNext: wireframe.share)
            .disposed(by: bag)
        
        input.posterPressed
            .asObservable()
            .withLatestFrom(Observable.just(interactor.movie))
            .subscribe(onNext: wireframe.showPoster)
            .disposed(by: bag)
        
        let title = details.map { $0.title }
        let image = details.map { $0.poster }
        let released = details.map { $0.details?.released ?? "" }
        let duration = details.map { $0.details?.duration ?? "" }
        let genre = details.map { $0.details?.genre ?? "" }
        let plot = details.map { $0.details?.plot ?? "" }
        let availableWebsite = details.map { $0.details?.web != nil }
        
        return MovieDetailPresenterOutput(loading: activityTracker.asDriver(),
                                          title: title,
                                          image: image,
                                          released: released,
                                          duration: duration,
                                          genre: genre,
                                          plot: plot,
                                          showError: errorTracker.asDriver(),
                                          availableWebsite: availableWebsite)
    }
}

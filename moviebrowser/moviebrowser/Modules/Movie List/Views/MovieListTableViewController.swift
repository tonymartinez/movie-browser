//
//  MovieListTableViewController.swift
//  moviebrowser
//
//  Created by Tony Martínez on 31/10/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MovieListTableViewController: UITableViewController {
    private let presenter: MovieListPresenterType
    private let searchController: UISearchController
    private let bag = DisposeBag()
    
    init(presenter: MovieListPresenter, searchController: UISearchController) {
        self.presenter = presenter
        self.searchController = searchController
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        configureTableView()
        bindPresenter()
    }
    
    private func configureNavigationBar() {
        navigationItem.title = "Movie Browser"
        navigationItem.hidesSearchBarWhenScrolling = false
        
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        
        definesPresentationContext = true
    }
    
    private func configureTableView() {
        tableView.register(UINib(nibName: MovieCell.reuseID, bundle: nil),
                           forCellReuseIdentifier: MovieCell.reuseID)
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        
        // Necesario si queremos utilizar Rx para implementar el DataSource del tableView
        tableView.dataSource = nil
    }
    
    private func bindPresenter() {
        let text = searchController.searchBar.rx.text.orEmpty
            .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
        
        let searchButtonClicked = searchController.searchBar.rx
            .searchButtonClicked
            .withLatestFrom(searchController.searchBar.rx.text.orEmpty)
        
        let query = Observable.merge(text, searchButtonClicked).distinctUntilChanged()
        let itemSelected = tableView.rx.itemSelected.asObservable()
        let input = MovieListPresenterInput(query: query,
                                            itemSelected: itemSelected)
        
        let output = presenter.transform(input: input)
        output.movies
            .drive(tableView.rx
                .items(cellIdentifier: MovieCell.reuseID,
                       cellType: MovieCell.self)) { _, presenter, cell in
                        cell.bind(presenter: presenter)
        }.disposed(by: bag)
        
        output.searching
            .drive(UIApplication.shared.rx.isNetworkActivityIndicatorVisible)
            .disposed(by: bag)
        
        output.showError
            .drive(onNext: showError)
            .disposed(by: bag)
    }
}

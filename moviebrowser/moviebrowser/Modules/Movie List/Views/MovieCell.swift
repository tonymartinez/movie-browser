//
//  MovieCell.swift
//  moviebrowser
//
//  Created by Tony Martínez on 31/10/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import UIKit
import Kingfisher

class MovieCell: UITableViewCell, Reusable {
    @IBOutlet weak var leftImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    
    deinit {
        leftImageView.kf.cancelDownloadTask()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        leftImageView.kf.cancelDownloadTask()
        leftImageView.image = nil
    }
    
    func bind(presenter: MovieCellPresenter) {
        titleLabel.text = presenter.title
        yearLabel.text = presenter.year
        
        let processor = DownsamplingImageProcessor(size: leftImageView.bounds.size)
        leftImageView.kf.setImage(
            with: presenter.image,
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(0.5))
            ])
    }
}

//
//  MovieListWireframe.swift
//  moviebrowser
//
//  Created by Tony Martínez on 31/10/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import UIKit

class MovieListWireframe: MovieListWireframeType {
    private let navigator = UINavigationController()
    private let service = WebService(configuration: OMBDConfig())
    
    func createModule() -> UIViewController {
        let interactor = MovieListInteractor(service: service)
        let presenter = MovieListPresenter(interactor: interactor, wireframe: self)
        
        let searchController = UISearchController(searchResultsController: nil)
        let view = MovieListTableViewController(presenter: presenter, searchController: searchController)
        
        navigator.viewControllers = [view]
        return navigator
    }
    
    func showMovieDetail(_ movie: Movie) {
        let wireframe = MovieDetailWireframe(navigator: navigator, service: service)
        let view = wireframe.createModule(with: movie)
        navigator.pushViewController(view, animated: true)
    }
}

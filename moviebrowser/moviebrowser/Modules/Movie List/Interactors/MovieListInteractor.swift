//
//  MovieListInteractor.swift
//  moviebrowser
//
//  Created by Tony Martínez on 31/10/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import RxSwift

class MovieListInteractor: MovieListInteractorType {
    private let service: WebService
    private var movies: [Movie] = []
    
    init(service: WebService) {
        self.service = service
    }
    
    func movies(for query: String) -> Observable<[Movie]> {
        let trimmed = query.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        guard !trimmed.isEmpty else { return Observable.just([]) }
        
        let endpoint = SearchEndpoint(query: query)
        return service
            .rx_requestObject(endpoint)
            .map { (response: SearchResponse) -> [Movie] in
                return response.movies
        }.do(onNext: { [weak self] in
            self?.movies = $0
        }).asObservable()
    }
    
    func movie(at index: Int) -> Observable<Movie> {
        return Observable.just(movies[index])
    }
}

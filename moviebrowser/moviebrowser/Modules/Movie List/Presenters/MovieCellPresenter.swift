//
//  MovieCellPresenter.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation

class MovieCellPresenter {
    let title: String
    let year: String
    let image: URL?
    
    let movie: Movie
    
    init(movie: Movie) {
        title = movie.title
        year = movie.year
        image = movie.poster
        
        self.movie = movie
    }
}

//
//  MovieListPresenter.swift
//  moviebrowser
//
//  Created by Tony Martínez on 31/10/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class MovieListPresenter: MovieListPresenterType {
    private let interactor: MovieListInteractorType
    private let wireframe: MovieListWireframeType
    private let bag = DisposeBag()
    
    init(interactor: MovieListInteractorType, wireframe: MovieListWireframeType) {
        self.interactor = interactor
        self.wireframe = wireframe
    }
    
    func transform(input: MovieListPresenterInput) -> MovieListPresenterOutput {
        let activityTracker = ActivityTracker()
        let errorTracker = ErrorTracker()
        
        let movies = input.query.flatMapLatest { [unowned self] in
            return self.interactor.movies(for: $0)
                .trackActivity(activityTracker)
                .trackError(errorTracker)
                .catchErrorJustReturn([])
                .map { return $0.map { MovieCellPresenter(movie: $0) } }
        }.asDriverOnErrorJustComplete()
        
        input.itemSelected.withLatestFrom(movies) { (indexPath, movies) -> Movie in
            return movies[indexPath.row].movie
        }.do(onNext: wireframe.showMovieDetail)
            .subscribe()
            .disposed(by: bag)
        
        return MovieListPresenterOutput(
            searching: activityTracker.asDriver(),
            movies: movies,
            showError: errorTracker.asDriver()
        )
    }
}

//
//  MovieListPresenterInput.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct MovieListPresenterInput {
    let query: Observable<String>
    let itemSelected: Observable<IndexPath>
}

struct MovieListPresenterOutput {
    let searching: Driver<Bool>
    let movies: Driver<[MovieCellPresenter]>
    let showError: Driver<Error>
}

protocol MovieListPresenterType {
    func transform(input: MovieListPresenterInput) -> MovieListPresenterOutput
}

//
//  PosterPreviewWireframe.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import UIKit

class PosterPreviewWireframe: PosterPreviewWireframeType {
    private let navigator: UINavigationController
    
    init(navigator: UINavigationController) {
        self.navigator = navigator
    }
    
    func createModule(movie: Movie) -> UIViewController {
        let interactor = PosterPreviewInteractor(movie: movie)
        let presenter = PosterPreviewPresenter(interactor: interactor, wireframe: self)
        let view = PosterPreviewViewController(presenter: presenter)
        
        return view
    }
    
    func dismissPreview() {
        navigator.dismiss(animated: true, completion: nil)
    }
}

//
//  PosterPreviewInteractor.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import RxSwift

enum SavePreviewError: Error {
    case PermissionError
}

class PosterPreviewInteractor: PosterPreviewInteractorType {
    private let movie: Movie
    
    init(movie: Movie) {
        self.movie = movie
    }
    
    func posterURL() -> Observable<URL?> {
        return Observable.just(movie.poster)
    }
}

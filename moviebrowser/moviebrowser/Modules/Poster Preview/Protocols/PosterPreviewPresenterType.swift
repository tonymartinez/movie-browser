//
//  PosterPreviewPresenterType.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct PosterPreviewPresenterInput {
    let close: Driver<Void>
}

struct PosterPreviewPresenterOutput {
    let image: Driver<URL?>
}

protocol PosterPreviewPresenterType {
    func transform(input: PosterPreviewPresenterInput) -> PosterPreviewPresenterOutput
}

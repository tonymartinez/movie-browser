//
//  PosterPreviewWireframeType.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import UIKit

protocol PosterPreviewWireframeType {
    func createModule(movie: Movie) -> UIViewController
    func dismissPreview()
}

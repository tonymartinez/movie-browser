//
//  PosterPreviewViewController.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher

class PosterPreviewViewController: UIViewController {
    private let presenter: PosterPreviewPresenterType
    private let bag = DisposeBag()
    
    @IBOutlet weak var imageView: UIImageView!
    
    init(presenter: PosterPreviewPresenterType) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        bindPresenter()
    }
    
    private func configureNavigationBar() {
        navigationController?.navigationBar.barTintColor = .black
        navigationController?.navigationBar.isTranslucent = false
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Close",
                                                           style: .plain,
                                                           target: nil,
                                                           action: nil)
    }
    
    private func bindPresenter() {
        let close = navigationItem.leftBarButtonItem!.rx.tap.asDriver()
        
        let input = PosterPreviewPresenterInput(close: close)
        let output = presenter.transform(input: input)
        
        output.image
            .asObservable()
            .subscribe(onNext: showImage)
            .disposed(by: bag)
    }
    
    private func showImage(_ url: URL?) {
        imageView.kf.setImage(with: url)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

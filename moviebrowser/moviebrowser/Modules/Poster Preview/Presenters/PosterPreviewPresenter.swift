//
//  PosterPreviewPresenter.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import RxSwift

class PosterPreviewPresenter: PosterPreviewPresenterType {
    private let interactor: PosterPreviewInteractorType
    private let wireframe: PosterPreviewWireframeType
    private let bag = DisposeBag()
    
    init(interactor: PosterPreviewInteractorType, wireframe: PosterPreviewWireframeType) {
        self.interactor = interactor
        self.wireframe = wireframe
    }
    
    func transform(input: PosterPreviewPresenterInput) -> PosterPreviewPresenterOutput {
        input.close
            .asObservable()
            .subscribe(onNext: wireframe.dismissPreview)
            .disposed(by: bag)
        
        let image = interactor.posterURL().asDriverOnErrorJustComplete()
        return PosterPreviewPresenterOutput(image: image)
    }
}

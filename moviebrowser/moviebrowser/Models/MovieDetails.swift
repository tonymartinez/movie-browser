//
//  MovieDetails.swift
//  moviebrowser
//
//  Created by Tony Martínez on 01/11/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import ObjectMapper

class MovieDetails: Mappable {
    private static let IMDB_MovieURL = URL(string: "https://www.imdb.com/title")!
    
    private(set) var released: String = ""
    private(set) var duration: String = ""
    private(set) var genre: String = ""
    private(set) var plot: String = ""
    private(set) var web: URL?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        released <- map["Released"]
        duration <- map["Runtime"]
        genre <- map["Genre"]
        plot <- map["Plot"]
        
        // Si la web no devuelve url de la película, montamos url de IMDB
        if let urlString = map["Website"].currentValue as? String,
            urlString != "N/A" {
            web = URL(string: urlString)
        } else if let imdbID = map["imdbID"].currentValue as? String {
            web = MovieDetails.IMDB_MovieURL.appendingPathComponent(imdbID)
        } else {
            web = nil
        }
    }
}

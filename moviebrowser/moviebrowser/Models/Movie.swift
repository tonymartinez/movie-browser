//
//  Movie.swift
//  moviebrowser
//
//  Created by Tony Martínez on 31/10/2019.
//  Copyright © 2019 sdos. All rights reserved.
//

import Foundation
import ObjectMapper

class Movie: Mappable {
    private(set) var title: String = ""
    private(set) var year: String = ""
    private(set) var poster: URL?
    private(set) var imdbID: String = ""
    var details: MovieDetails?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        title <- map["Title"]
        year <- map["Year"]
        poster <- (map["Poster"], URLTransform())
        imdbID <- map["imdbID"]
        
        details = MovieDetails(JSON: map.JSON)
    }
}

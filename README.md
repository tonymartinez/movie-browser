# Version de Xcode

Xcode 11.1

# Rama donde se encuentra el código

El último código se encuentra tanto en la rama develop como en master. También se ha creado una tag v0.1 que apunta a la última versión del código. Se ha utilizado gitflow para la gestión de ramas.

# Antes de compilar

```
pod install
```

# Pods utilizados

- Alamofire + ObjectMapper: para consumir web services y parsear las respuestas
- RxSwift + RxCocoa + RxGesture: frameworks de Rx para programación reactiva
- Kingfisher: para descarga y cacheo de imágenes

# Arquitectura utilizada

Se ha utilizado la arquitectura VIPER. Para la comunicación entre capas se ha utilizado Rx de forma intensiva. El código de la aplicación sigue el paradigma declarativo que proporciona Rx. Esto se puede ver claramente en vistas y presenters.

Para desacoplar las capas se han utilizado protocolos. Esto es, para implementar wireframes, interactors y presenters hay que heredar del protocolo correspondiente.

La vista es un ente completamente pasivo. Sólo se preocupa de dos cosas: 
	- Los eventos de usuario para notificar al presenter
	- Bindeo de los datos que le pasa el presenter hacia los componentes gráficos

Los presenters funcionan bajo un mecanismo "input-output". Esto quiere decir que recogen los inputs de la vista, los procesan ejecutando los casos de uso que implementa el interactor correspondiente y devuelven los outpus hacia la vista.

Los interactors se han dejado lo más sencillos posible. Sólamente implementan los casos de uso necesarios, esto es, la lógica de negocio. Para testear la lógica de negocio bajo el paradigma Rx se puede utilizar RxTest y RxBlocking.

Los wireframe se encargan de montar los módulos y de la navegación entre pantallas. A la hora de cargar los módulos no se ha utilizado ningún framework de inyección de dependencias (por simplicidad).

La clase WebService puede ser instanciada con la configuración que necesitemos para acceder a cualquier web service. Por simplicidad, el protocolo Endpoint se ha mantenido lo más sencillo posible, se ha codificado lo justo y necesario para poder utilizar el web service de www.imdbapi.com. Este web service no tiene diversos endpoints con sus subpaths. Lo normal es que un web service implemente diferentes endpoints/subpaths, por tanto habría que extender el protocolo Endpoint para soportar esto.

El modelo de dominio consta de 2 clases: Movie y MovieDetails. Se podía haber mantenido sólo la clase Movie, pero se ha preferido mantener modelos de datos con pocos atributos.

# Algunas consideraciones

Las configuraciones necesarias están en el directorio Config. Las únicas configuraciones que he incluido son aquellas relativas al web service utilizado (base url, apikey y timeout de las peticiones). Estas configuraciones también se pueden incluir a nivel de ficheros xcconfig pero no he visto necesario complicar el ejemplo más de lo necesario. Además, tampoco he creado varios targets puesto que solo disponía de un entorno de pruebas (el que ofrece imdbapi.com).

No se han localizados los strings por no encontrarlo necesario de cara a este ejemplo. En proyectos reales conviene siempre localizar los textos, aunque sólo vayamos a dar soporte a un idioma.

No se ha incluido capa de persistencia dado que no he visto necesario incluir más complejidad de la necesaria. En caso de que se quisiera incluir, habría que:

1. Meter una capa por encima del Interactor, llamémosla Repository (podemos utilizar genéricos para hacerla más flexible a los cambios de tecnologías).
2. Esta capa accedería a los datos utilizando la tecnología de BD que queramos: Core Data, Realm, sqlite, etc...
3. Es muy importante que la tecnología de BD que utilicemos no salga del Repository porque así desacoplamos la tecnología de BD con respecto a nuestra lógica de negocio (Interactors)
4. La capa Repository debe devolver objetos de dominio (PONSO), por lo que habría que implementar un mecanismo de mappers. Por ejemplo, una capa de mappers que hagan la traducción entre Realm -> PONSO y PONSO -> Realm.


No se ha desarrollado el guardado de imágenes en la galería por falta de tiempo. Para esto, simplemente hay que añadir un nuevo caso de uso (nueva función en el protocolo PosterPreviewInteractorType). La lógica de negocio consistiría en pedir al sistema acceso a la galería y proceder al guardado de la imagen. A nivel de vista solamente incluir un nuevo botón que dispare la acción de guardado. El presenter sólo se encargaría de comunicar ambas capas (vista y lógica de negocio).

Tampoco se ha desarrollado el copiado del texto al portapapeles. En este caso, en mi opinión, esta lógica podría hacerse directamente en la vista ya que la clase Clipboard que maneja Apple tira del framework UIKit y la idea es no tocar UIKit ni en los presenters ni en los interactors. 


